package org.epam;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for ParserLibrary
 */
public class AppTest
{
    @Test
    public void shouldGetQuoteByNumber123()
    {
        assertEquals( "<}{Lighter> знаете что такое линукс?\n<}{Lighter> мне тут юзверь выдал" +
                "\n<}{Lighter> " +
                "\"ну это же типо виндовз," +
                " но программы надо выполнять, набором букв, а не мышкой щёлкать\"",new Quotes(123).getQuotesText());
    }

    @Test
    public void shouldGetQuoteByNumber454600()
    {
        assertEquals( "ххх: расскажите пожалуйста про свою диету. Что нужно кушать что бы снились такие интересные сны?\n" +
                "\n" +
                "yyy: этим и отличаются писатели - с умом, который умеет придумывать, от потребителей, которым для придумывания нужны наркотики.\n" +
                "\n" +
                "zzz: yyy, как вы странно и длинно сказали \"своей дури хватает\".",new Quotes(454600).getQuotesText());
    }

    @Test
    public void shouldGetQuoteByNumber131()
    {
        assertEquals( "<kanashigeni> филия, а на хвост мой посмотри, ня? %)\n" +
                "<Pucca> вижу :)\n" +
                "<StrangeCat> как не стыдно хвост показывать! :р\n" +
                "--> Бесхвостый (__________@76-138.kharkov.dialup.ukr.net) has joined #anime\n" +
                "<kanashigeni> а чего хвоста стыдиться?\n" +
                "<Бесхвостый> А кто сказал, что я его стыжусь? Его просто нет. :)\n", new Quotes(131).getQuotesText());
    }


    String regexpSearchHtmlEntities = "((.|\n)*<br>(.|\n)*)|((.|\n)*&lt;(.|\n)*)|((.|\n)*&gt;(.|\n)*)" +
            "|((.|\n)*&amp;(.|\n)*)|((.|\n)*&quot;(.|\n)*)";

    @Test
    public void shouldGetQuoteWithoutHtmlEntities_Null()
    {
        Quotes page = new Quotes();
        assertEquals(page.getQuotesText(), page.getQuotesText(page.getNumberPage()));
    }

    @Test
    public void shouldGetQuoteWithoutHtmlEntities_quote21()
    {
        assertFalse(new Quotes(21).getQuotesText().matches(regexpSearchHtmlEntities));
    }

    @Test
    public void shouldGetQuoteWithoutHtmlEntities_quote133()
    {
        assertFalse(new Quotes(133).getQuotesText().matches(regexpSearchHtmlEntities));
    }

    @Test
    public void shouldGetQuoteWithoutHtmlEntities_quote1()
    {
        assertFalse(new Quotes(1).getQuotesText().matches(regexpSearchHtmlEntities));
    }
}

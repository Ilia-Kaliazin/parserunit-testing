package org.epam;

import org.epam.Exeptions.NotFound;
import java.util.*;

public class App {
    /**
     * Complete programm
     */
    private static boolean complete = false;

    /**
     * Language notice
     */
    private static List<String> language = new ArrayList<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        NotFound numberTemp = new NotFound();
        Quotes pageTemp = new Quotes();
        int lastNumberPage = -1;

        /**
         * Init language notice
         */
        {
            System.out.println("Hello! This programm parser page.");
            language.add("Error! You entered the number incorrectly.");
            language.add("Error! Page not found(404).");
        }

        /**
         * Main
         */
        do {
            while (true) {
                System.out.print("Input a number: ");
                String tempString = in.next();

                try {
                    if (pageTemp.setNumberPage(Integer.parseInt(tempString))) {

                        if (confirm("Your number: " + pageTemp.getNumberPage(), in)) {

                            if (numberTemp.checkNumberPage(pageTemp.getNumberPage()) && lastNumberPage != pageTemp.getNumberPage()) {
                                System.out.println(language.get(1));
                                break;
                            } else {
                                System.out.println("Okey, loading..\n");
                                lastNumberPage = pageTemp.getNumberPage();
                                // Получение цитаты.


                                /**    */
                                String quotesText = pageTemp.getQuotesText();
                                if(quotesText == null) {
                                    numberTemp.addNumberPage(pageTemp.getNumberPage());
                                    System.out.println(language.get(1));
                                } else if (quotesText.equals("Out of connection.")) {
                                    System.out.println("Server out of connection.");
                                } else {
                                    numberTemp.delNumberPage(pageTemp.getNumberPage());
                                    System.out.println(quotesText);
                                }
                            }
                        } else if (confirm("Revert?", in)) break;
                    } else {
                        System.out.println(language.get(0));
                        break;
                    }
                } catch (Exception e) {
                    System.out.println(language.get(0));
                    break;
                }

                if (!confirm("\nExit?", in)) break;
                else {
                    numberTemp.listNotFound();
                    complete = !complete;
                    break;
                }
            }

        } while (!complete);
        System.out.println("Buy buy. ;-)");
        in.close();
    }

    /** Answers on questions */
    public static boolean confirm(String textLine, Scanner in) {
        while (true) {
            System.out.println(textLine + "\nYes / No?");

            try {
                String tempString = in.next();

                if (tempString.toLowerCase().equals("yes"))
                    return true;
                else if (tempString.toLowerCase().equals("no"))
                    return false;

            } catch (Exception e) {
                System.out.println("Exeption: " + e);
            }
        }
    }
}